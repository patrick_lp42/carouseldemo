(function(){
	"use strict";
	var url1 = "/admin/index/getAllAanalyzeData.json";
	var param1 = {};
	var type1 = "GET";
	var datatype1 = "json";
	var callback1 = function getAllAanalyzeData(data){
		var queryResult = data;
		var iData = queryResult.result; //获取result
		var admin = iData[0].admin; //获取用户信息

		var real_name = admin.real_name;
		$(".hello_client .hello_word strong").text(real_name);

		var dealerNVCount = iData[0].dealerNVCount;
		$(".dealerNVCount strong").text(dealerNVCount);
		var newsNVCount = iData[0].newsNVCount;
		$(".newsNVCount strong").text(newsNVCount);
		var activityNVCount = iData[0].activityNVCount;
		$(".activityNVCount strong").text(activityNVCount);
		var orderCount = iData[0].orderCount;
		$(".orderCount strong").text(orderCount);

		var dealerCount = iData[0].dealerCount;
		$(".dealerCount strong").text(dealerCount);
		var newsCount = iData[0].newsCount;
		$(".newsCount strong").text(newsCount);
		var activityCount = iData[0].activityCount;
		$(".activityCount strong").text(activityCount);

		//显示首页统计
		$(".index_content")[0].style.visibility = "visible";
	}
	var _jsonAjax1 = jsonAjax(url1, param1, type1, datatype1, callback1);
});
