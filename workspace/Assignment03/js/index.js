"use strict";
/*ajax接口通用方法*/
function jsonAjax(url, param, type, datatype, callback) {  
	$.ajax({  
		type: type,  
		url: url,  
		data: param,  
		dataType: datatype,  
		success: callback,  
		error: function () {  
			alert("接口报错");
		}
	})
} 

