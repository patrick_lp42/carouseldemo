/*
*	switchable  
* 	Author:penglei
*	new switchable(nodes,options);
*   传入参数：
*	//nodes {ctrls（控制器即tab容器）,contents（每个tab对应的内容容器）}
*	//options{css（样式classname）,eventName（切换event事件类型，如mousemove，click）,interval（连续播放间隔，暂无）,delay（延迟响应）}
*	return {elems ,param,show ,hide ,trans,prev,next,addEvents,init}
* 	返回参数：
*	elems : {
* 		ctrls:控制器；传入参数nodes.ctrls
*       contents:对应容器；传入参数nodes.contents
* 	}
* 	param : {
* 	 total：控制器总个数
*    curIndex ：当前tab的标号
*    timmer ：延迟响应的timmer函数
* 	 onHandle ： 切换后的回调函数，用于自定义切换效果
*  }
*  show ：显示切换项
*  hide ：隐藏切换项
*  trans ：内部tabable响应函数
*  prev ： tab上翻（切换到当前tab的前一个，当前第一个时，切换到最后一个）
*  next ：tab下翻（切换到当前tab的下一个，当前最后一个时，切换第一个）
*  addEvents : tabs的事件初始化
*  init : 初始化
*/
(function(win){
	win.switchable = function(nodes,options){
		var elems={},
			param = adapterParam(options);	
		//辅助函数
		var addEvent;//事件绑定
		if(window.attachEvent){
			addEvent = function(o,eventname,handle,args){
				o.attachEvent("on"+eventname,function(e){
					handle(e,args);
				});			
			}
		}else{
			addEvent = function(o,eventname,handle,args){
				o.addEventListener(eventname, function(e){
					handle(e,args);
				}, false);			
			}			
		}		
		function hasclassName(o,_css){
			var c = " "+o.className+" ";
			if(-1 === c.indexOf(" "+_css+" ")){
				return false;
			}
			return true;			
		}
		
		function addClass(o,_css) {
			if(!hasclassName(o,_css)){
				o.className +=" "+ _css; 
			}
		}
		
		function removeClass(o,_css){
			if(hasclassName(o,_css)){
				o.className = trim((" "+o.className+" ").replace(" "+_css+" "," "));
			}	
		}
		
		function removeNodsClass(o,_css){//批量去除class
			for (var i=0; i < o.length; i++) {
			   removeClass(o[i],_css); 
			};		
		}
		
		function trim(str){
			return str.replace(/^[\s\xA0]+/,'').replace(/[\s\xA0]+$/,'');
		}
		
		function clearTimeoutTimmer(t){
			if(t!=null){
				clearTimeout(t);
				t=null;
			}
		}
		function clearIntervalTimmer(t){
			if(t!=null){
				clearInterval(t);
				t=null;
			}
		}		
		function adapterParam(options){
			var param = {};
			param.total = nodes.contents.length;
			// if (nodes.ctrls && param.total !== nodes.ctrls.length) {		// 检查控制项和内容项是否能一一匹配
				// throw "can not match ctrls(" + nodes.ctrls.length + ") and contents(" + param.total + ")";
			// }
			param.curIndex = options.curIndex||-1;		// 当前项，初始化时为0
			elems.ctrls = nodes.ctrls;
			elems.contents = nodes.contents;
			param.css = options.css;
			param.eventName = options.eventName || "mouseover";
			param.interval = options.interval;		// 播放间隔
			param.delay = options.delay;			// 触发延迟	
			
			param._ctrlTimmer = null;//操作延迟，timmer
			param._replayTimmer = null;//自动播放，timmer
			param.onHandle = options.onHandle;
			return param;
		}
				
		function show(ctr,content){		
			// 给下一项添加样式
			if(ctr){
			   addClass(ctr, param.css);
			}		
			addClass(content, param.css);			
		}
		
		function hide(ctrs,contents){
			// 移除当前项样式
			ctrs&&removeNodsClass(ctrs, param.css);
			contents&&removeNodsClass(contents, param.css);		
		}
		
		function prev(){
			var index = (param.curIndex - 1) < 0? param.total - 1 : param.curIndex - 1;
			trans(index);
		}
	
		function next(){
			var index = (param.curIndex + 1) == param.total ? 0 : param.curIndex + 1;
			trans(index);
		}	
		
		function trans(index){//切换响应
			
			index = index < 0 ? 0 : (index >= param.total ? param.total - 1 : index);		// 修正index的值
			var ctrl = elems.ctrls ? elems.ctrls[index] : null,
				  content = elems.contents[index];
			if (-1 === param.curIndex) { param.curIndex = 0; }
			
			//清楚当前样式
			hide(elems.ctrls,elems.contents);
			//切换tab
			show(ctrl,content);
			//扩展接口
			param.onHandle && param.onHandle(index, ctrl, content);	
									
			param.curIndex = index;
		}
		
		function addEvents(){
			if (elems.ctrls && elems.ctrls.length && param.eventName) {
				var change;
				if(typeof param.delay !="undefined"){
					param._ctrlTimmer = null;
					change = function(e, index){
						clearTimeoutTimmer(param._ctrlTimmer);
						param._ctrlTimmer = setTimeout(function(){
							trans(index);
						},param.delay);	
					}				
				}else{
					change = function(e, index){
						trans(index);			
					}	
				}				
				for (var i = param.total - 1; i >= 0; i--) {
					addEvent(elems.ctrls[i], param.eventName, change, new Number(i));
				}	
			}				
		}
		function replayBind(){
			//添加自动播放
			if(param.interval){
				setReplayTimmer();			
				var targets = [];
				elems.contents && (targets = targets.concat(elems.contents));
				elems.ctrls && (targets = targets.concat(elems.ctrls));
				for(var i =0;i<targets.length;i++ ){
					addEvent(targets[i],"mouseover",clearReplayTimmer);
					addEvent(targets[i],"mouseout",setReplayTimmer);
				}			
			}			
		}
		function setReplayTimmer(){
			if(param._replayTimmer!=null){
				clearReplayTimmer();
			}
			param._replayTimmer = setInterval(
				function(){
					next();
				},
				param.interval
			);
		}
		function clearReplayTimmer(){
			clearIntervalTimmer(param._replayTimmer);
		}
		function init(){
			//绑定事件		
			addEvents();
			//绑定循环播放
			replayBind();
			trans(param.curIndex);
		}
					
		init();
		
		return {
			elems : elems,
			param : param,
			stopReplay:clearReplayTimmer,
			initReplay:setReplayTimmer,
			show : show,
			hide : hide,
			trans : trans,
			prev : prev,
			next : next,
			addEvents : addEvents,
			init : init
		};
	}	
})(window);

function stopDefault(e){
	if(e && e.preventDefault){
		e.preventDefault();
	}else{
		window.event.returnValue = false;
	}

	return false;
}

function focusScroll(nodesrc,options){
	var nodeSrc = {};
	var Options = {
			cName : "current",
			eName : "mouseover",
			delay : 300,
			scroll_inc : 220
		};
	nodeSrc = param(nodeSrc,nodesrc);
	Options = param(Options,options);

	var s_timmer = null;
	var contents_len = nodeSrc.contents.length;

	if(nodeSrc.index_box){ nodeSrc.index_box.innerHTML = 1;}
	if(nodeSrc.num_box){ nodeSrc.num_box.innerHTML = contents_len;}

	var ctrls = nodeSrc.ctrls ? nodeSrc.ctrls : "";
	var nodes = {
			ctrls : ctrls ,
			contents : nodesrc.contents				
		},
		params = {
			css : Options.cName, //contents，ctrls当前添加样式名
			eventName : Options.eName,//控制器触发事件名
			delay : 100, //可选项，控制器触发延迟	
			onHandle : function(index){ //可选项，回调函数，可用于做渐变效果
				scrollmove(index,Options.scroll_inc,nodeSrc.scrollBody,Options.delay);
			}
		};		
	var _switch = switchable(nodes,params);

	nodeSrc.panel_cnt.addCss({width:nodeSrc.contents.length*Options.scroll_inc+"px"});
	nodeSrc.scrollBody.scrollLeft = _switch.param.curIndex*Options.scroll_inc;

	function tweenFunc(t,b,c,d){ return -c * Math.cos(t/d * (Math.PI/2)) + c + b; }//二次缓动

	function scrollmove(index,scroll_inc,scroll,delay){
		if(s_timmer!=null){
			clearInterval(s_timmer);
			s_timmer=null;
		}	 	
		var s1=index*scroll_inc,s0=scroll.scrollLeft,s=0;
		var t0 = (new Date()).getTime();
		var tchange = 0;
		if(nodeSrc.index_box){ nodeSrc.index_box.innerHTML = index+1;}
		s_timmer = setInterval(function(){
			tchange = (new Date()).getTime()-t0;
			s = tweenFunc(tchange,s0,s1-s0,delay);
			if(tchange>delay){
				scroll.scrollLeft = s1;
				clearInterval(s_timmer);
				s_timmer = null;
				return;
			}				
			scroll.scrollLeft = s;
		},13);
	}	

	function param(defJson,userJson){
		for(var key in userJson){
			if(typeof userJson[key] != undefined){
				if(typeof defJson[key] != undefined){
					defJson[key] = userJson[key];
				}else{
					defJson.key = userJson[key];
				}
			}
		}
		return defJson;
	}

	return _switch;
}

function scroll_switch(slide_box,delay){
	var scroll_body = slide_box.scroll_body,
		panel_cnt = slide_box.panel_cnt,
		ctrls = slide_box.ctrls,
		contents = slide_box.contents,
		next_ctrl = slide_box.next_ctrl,
		prev_ctrl = slide_box.prev_ctrl,
		scroll_inc = slide_box.scroll_inc,
		s_timmer = null;

	var nodes = {
			ctrls : ctrls,
			contents : contents				
		},
		params = {
			css : "current",//contents，ctrls当前添加样式名
			eventName : "mouseover",//控制器触发事件名
			delay : 100,//可选项，控制器触发延迟	
			onHandle : function(index){//可选项，回调函数，可用于做渐变效果
				scrollmove(index,scroll_body,delay);
			}
		};		
	var _switch = switchable(nodes,params);
	
	panel_cnt.addCss({width:contents.length*scroll_inc+"px"});
	
	//上下翻
	next_ctrl.addEvent('click',function(e){
		stopDefault(e);
		_switch.next();
	});
	prev_ctrl.addEvent('click',function(e){
		stopDefault(e);
		_switch.prev();
	});
	
	function tweenFunc(t,b,c,d){ return -c * Math.cos(t/d * (Math.PI/2)) + c + b; }//二次缓动

	function scrollmove(index,scroll,delay){
		if(s_timmer!=null){
			clearInterval(s_timmer);
			s_timmer=null;
		}	 	
		var t = 0,s1=index*scroll_inc,s0=scroll.scrollLeft,s=0;
		s_timmer = setInterval(function(){
			s = tweenFunc(t,s0,s1-s0,delay);
			if(t>delay){
				scroll.scrollLeft = s1;
				clearInterval(s_timmer);
				s_timmer = null;
				return;
			}				
			scroll.scrollLeft = s;
			t+=13;
		},13);
	}		
}








