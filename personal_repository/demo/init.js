(function(){
	//轮播
	$(".mod_focus").each(function(){
		var obj2 = $(this),
			prev_ctrl = obj2.$(".focus_prev"),
			next_ctrl = obj2.$(".focus_next");
		var nodesrc1 = {
			scrollSrc : obj2,
			scrollBody : obj2.$(".focus_body").$(0),
			panel_cnt : obj2.$(".focus_body ul").$(0),
			contents : obj2.$(".focus_body ul li"),
			ctrls : obj2.$(".focus_ctrl span")
		}
		var options1 = {
			scroll_inc : obj2.$(".focus_body .current").$(0).offsetWidth
		}
		var _focusScroll1 = focusScroll(nodesrc1,options1);

		var s_timmer2 = null;


		s_timmer2 = setInterval(function(){
			_focusScroll1.next();
		},5000);

		obj2.addEvent("mouseover",function(){
			clearInterval(s_timmer2);
		}).addEvent("mouseout",function(){
			s_timmer2 = setInterval(function(){
				_focusScroll1.next();
			},5000);
		})

		next_ctrl.addEvent('click',function(e){
			stopDefault(e);
			_focusScroll1.next();
		});
		prev_ctrl.addEvent('click',function(e){
			stopDefault(e);
			_focusScroll1.prev();
		});
	});

})($);
